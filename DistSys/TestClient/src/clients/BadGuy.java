package clients;

import dl.ResourceOrderException;

public class BadGuy extends TestClient {

    public BadGuy(int selfID, int nodeNumber, int resourceLength, boolean persistent) {
	super(selfID, nodeNumber, persistent);
	this.resources = TestClient.buildBadGuyResources(resourceLength);
    }

    @Override
    public void run() {
	do {
	    try {
		super.run();
	    } catch (ResourceOrderException e) {
		System.out.println("Resource Order is wrong restarting: "+e.getMessage());
	    }
	} while (persistent);
    }
}
