package clients;

public class GoodGuy extends TestClient {

    public GoodGuy(int selfID, int nodeNumber, int resourceLength, boolean persistent) {
	super(selfID, nodeNumber, persistent);
	this.resources = TestClient.buildGoodGuyResources(resourceLength);
    }

}
