package clients;

import java.util.LinkedList;

import dl.StateMachine;

public class TestClient extends Thread {
    
    protected StateMachine sm;
    protected int selfID;
    protected int nodeNumber;
    protected LinkedList<Integer> resources;
    protected boolean persistent;
    private int holdTime;

    public TestClient(int selfID, int nodeNumber, boolean persistent) {
	this.selfID = selfID;
	this.nodeNumber = nodeNumber;
	this.persistent = persistent;
	sm = new StateMachine(selfID, nodeNumber);
    }

    public TestClient(int selfID, int nodeNumber, boolean persistent, int res[]) {
	this.selfID = selfID;
	this.nodeNumber = nodeNumber;
	this.persistent = persistent;
	sm = new StateMachine(selfID, nodeNumber);
	this.resources = new LinkedList<Integer>();
	for (int n : res) {
	    resources.addLast(n);
	}
	this.holdTime = -1;
    }
    public TestClient(int selfID, int nodeNumber, boolean persistent, int res[],int holdTime) {
	this(selfID,nodeNumber,persistent,res);
	this.holdTime = holdTime;
    }
    @Override
    public void run() {
	LinkedList<Integer> tmpResources = new LinkedList<Integer>();
	tmpResources.addAll(resources);
	try {
	    while (tmpResources.size() != 0) {
		int value = tmpResources.pop();
		if (value > 0) {
		    System.out.println("[" + selfID + "] acquiring " + value);
		    sm.acquire(value);
		    System.out.println("[" + selfID + "] acquired " + value);
		} else {
		    value = -value;
		    System.out.println("[" + selfID + "] releasing " + value);
		    sm.release(value);
		    System.out.println("[" + selfID + "] released " + value);
		}
		if (persistent && tmpResources.size() == 0) {
		    tmpResources = new LinkedList<Integer>();
		    tmpResources.addAll(resources);
		}
		int time = 250;
		if ( holdTime == -1){
		    time += Math.random()*50000;
		}
		else {
		    time += holdTime;
		}
		Thread.sleep(time);
	    }
	} catch (InterruptedException e) {
	    new RuntimeException();
	}
	System.out.println("[" + selfID + "] DONE ");
	
    }

    protected static LinkedList<Integer> buildGoodGuyResources(int length) {
	int inc = 0;
	LinkedList<Integer> result = new LinkedList<Integer>();
	for (int i = 0; i < length; i++) {
	    inc += (int) (Math.random() * 3 + 1);
	    result.addLast(inc);
	}
	for (int k = 1; k <= length; k++) {
	    result.addLast(-result.get(length - k));
	}
	return result;
    }

    protected static LinkedList<Integer> buildBadGuyResources(int length) {
	int inc = 0;
	LinkedList<Integer> result = new LinkedList<Integer>();
	for (int i = 0; i < length; i++) {
	    inc += (int) (Math.random() * length);
	    result.push(inc);
	}
	for (int k = 1; k <= length; k++) {
	    result.addLast(-result.get(length - k));
	}
	return result;
    }

}
