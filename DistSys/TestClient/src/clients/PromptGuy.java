package clients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.LinkedList;

import dl.StateMachine;

public class PromptGuy {
    StateMachine sm;
    private int selfID;

    public PromptGuy( ) {
	
    }
    
  
    
    public void start(){
	setupStateMachine();

	while ( true ) {
	    int value = prompt("Resource");
	    if (value > 0){
		    System.out.println("[" + selfID + "] acquiring " + value);
		    sm.acquire(value);
		    System.out.println("[" + selfID + "] acquired " + value);
	    } else {
		    value = -value;
		    System.out.println("[" + selfID + "] releasing " + value);
		    sm.release(value);
		    System.out.println("[" + selfID + "] released " + value);
	    }
	}
	
    }
    private void setupStateMachine(){
	this.selfID = prompt("Node ID");
	int nodes = prompt("Number of Nodes");
	sm = new StateMachine(selfID, nodes);
    }
    
    public int prompt(String message)
    {
	String s = "";
	Integer result = null;
	do{
	    try {
        	    System.out.print(message);
        	    System.out.print("[quit]?> ");
        	    BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
        	    s = br.readLine();
        	    result = Integer.parseInt(s);
	    } catch (NumberFormatException e) {
		
	    } catch (IOException e) {

	    }
	} while (s.equals("quit") || result == null);
	
	if (s.equals("quit") ){
	    System.exit(0);
	}
    	return result;
    }
}
