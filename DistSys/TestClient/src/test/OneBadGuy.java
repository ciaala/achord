package test;

import java.util.LinkedList;

import clients.BadGuy;
import clients.GoodGuy;
import clients.TestClient;

public class OneBadGuy {

    public static void main(String[] args) {
	int nodes = 2;
	int resources = 8;
	LinkedList<TestClient> client_list = new LinkedList<TestClient>();
	for (int i = 0; i < nodes; i++) {
	    TestClient tc = new GoodGuy(i, nodes + 1, resources, false);
	    client_list.push(tc);
	}
	client_list.push(new BadGuy(nodes, nodes + 1, resources, true));
	try {
	    Thread.sleep(1000);
	    for (TestClient tc : client_list) {
		tc.start();
	    }
	} catch (RuntimeException e) {
	    System.out.println("[ERROR]");
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    

}
