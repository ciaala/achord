package test;

import java.util.LinkedList;

import clients.TestClient;

public class FixedGuys {
    public static void main(String[] args) {
	int nodes = 3;
	
	int guy0[] = { 1, 2, -2, -1 };
	int guy1[] = { 2, 3, 4, -4, -3, -2 };
	int guy2[] = { 3, 4, -4, -3 };

	LinkedList<TestClient> client_list = new LinkedList<TestClient>();

	client_list.push(new TestClient(0, nodes, true, guy0));
	client_list.push(new TestClient(1, nodes, true, guy1));
	client_list.push(new TestClient(2, nodes, true, guy2));
	try {
	    Thread.sleep(1000);
	    for (TestClient tc : client_list) {
		tc.start();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

}
