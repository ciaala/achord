package test;


import java.util.LinkedList;

import clients.GoodGuy;
import clients.TestClient;

public class AllGoodGuy {

    public static void main(String[] args) {
	int nodes = 4;
	int resources = 6;
	LinkedList<TestClient> client_list = new LinkedList<TestClient>();
	for (int i = 0; i < nodes; i++) {
	    TestClient tc = new GoodGuy(i, nodes, resources, true);
	    client_list.push(tc);
	}

	try {
	    Thread.sleep(1000);
	    for (TestClient tc : client_list) {
		tc.start();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

}
