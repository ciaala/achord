package dl;

public interface ResourceManager {	
	public void acquire(int ResourceID) throws InterruptedException, Exception; 
	public void release(int ResourceID); 
}

 