package dl;

import java.util.Collection;
import java.util.LinkedList;

import javax.jms.JMSException;
import javax.naming.NamingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class StateMachine implements ResourceManager {
    private enum State {
	WAIT, LAZY, LISTENING;

    }

    private Logger logger = Logger.getLogger("dl");
    private VectorClock vector;
    private VectorClock acquireVector;
    private PriorityControl priority;
    private int nodeNumber;
    private int currentAcquireCounter;
    private Enum<State> internalState;
    private LinkedList<Integer> resourceAcquired = new LinkedList<Integer>();
    private int currentResourceID;
    private TopicPublisherInterface publisher;
    private TopicSubscriberInterface subscriber;
    private int selfID;

    public StateMachine(int selfID, int nodeNumber) {
	// logger.setLevel(Level.ALL);
	this.selfID = selfID;
	this.nodeNumber = nodeNumber;
	this.priority = new PriorityControl(selfID);
	vector = new VectorClock(nodeNumber);
	acquireVector = new VectorClock(nodeNumber);
	currentAcquireCounter = -1;
	currentResourceID = -1;
	internalState = State.LAZY;
	try {
	    publisher = new ActiveMQPublisher(selfID);
	    subscriber = new ActiveMQSubscriber(selfID, this);
	    subscriber.start();
	} catch (NamingException e) {
	    throw new RuntimeException();
	} catch (JMSException e) {
	    throw new RuntimeException();
	}
    }

    public synchronized void acquire(int resourceID) throws ResourceOrderException {
	try {
	    if (internalState == State.WAIT) {
		logger.debug("ACQUIRE Error WAIT");
	    } else if (internalState == State.LAZY) {
		if (resourceAcquired.size() == 0) {
		    this.setupListening(resourceID);
		    // TODO DA METTERE FUORI ?????
		    this.wait();
		} else if (resourceAcquired.getFirst() < resourceID) {
		    this.setupListening(resourceID);
		    // TODO DA METTERE FUORI ?????
		    this.wait();
		} else {
		    logger.debug("ACQUIRE Error Resource Order");
		    logger.debug("ERROR Releasing Resources");

		    ResourceOrderException e = new ResourceOrderException(resourceID, resourceAcquired);
		    this.releaseAcquiredResources();
		    throw e;
		}
	    } else if (internalState == State.LISTENING) {
		logger.debug("ACQUIRE Error LISTENING");
	    }
	} catch (JMSException e) {
	    throw new RuntimeException("Network");
	} catch (InterruptedException e) {
	    throw new RuntimeException("Thread");
	}
    }

    public synchronized void release(int resourceID) throws ResourceOrderException {
	if (internalState == State.WAIT) {
	    logger.debug("RELEASE Error WAIT");
	} else if (internalState == State.LAZY) {
	    if (resourceAcquired.getFirst() == resourceID) {
		this.sendMessage(Protocol.NOTIFY, -1, resourceID);
		resourceAcquired.pop();
	    } else {
		ResourceOrderException e = new ResourceOrderException(resourceID, resourceAcquired);
		this.releaseAcquiredResources();
		throw e;
	    }
	} else if (internalState == State.LISTENING) {
	    logger.debug("RELEASE Error LISTENING");
	}

    }

    public synchronized void request(int resourceID, int nodeID, VectorClock msgVector) {
	vector.merge(msgVector);
	vector.increment(selfID);
	try {
	    if (internalState == State.WAIT) {
		this.denyIfHoldenResource(resourceID, nodeID);
	    } else if (internalState == State.LAZY) {
		this.denyIfHoldenResource(resourceID, nodeID);
	    } else if (internalState == State.LISTENING) {
		if (resourceID != currentResourceID) {
		    this.denyIfHoldenResource(resourceID, nodeID);
		} else {
		    Enum<Check> result = acquireVector.evaluate(this.selfID, nodeID, msgVector);
		    /** Stiamo contendo una risorsa */
		    logger.debug("*** Contention: " + result + "<" + selfID + "," + nodeID + ">");
		    if (result == Check.CONCURRENT) {
			if (priority.isWinner(nodeID)) {
			    /** Il remoto e' vincente su di noi ha un remote ID maggiore */
			    this.sendMessage(Protocol.ACKNOWLEDGE, nodeID, resourceID);
			} else {
			    /** Il remote e' perdente la priorita' e' nostra */
			    this.sendMessage(Protocol.NEGATE, nodeID, resourceID);
			}
		    }// Remote node is Younger
		    else if (result == Check.YOUNGER) {
			this.sendMessage(Protocol.NEGATE, nodeID, resourceID);
		    }
		    // else if (result == Check.OLDER) {
		    // internalState = State.WAIT;
		    // this.sendMessage(Protocol.ACKNOWLEDGE, nodeID, resourceID);
		    // }
		}
	    }
	} catch (JMSException e) {
	    throw new RuntimeException();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public synchronized void acknowledge(int ResourceID, int nodeID, VectorClock msgVector) {
	this.vector.merge(msgVector);
	this.vector.increment(selfID);
	if (internalState == State.WAIT) {

	} else if (internalState == State.LAZY) {
	    logger.debug("[" + selfID + "] Error Acknowledge received while in Lazy");
	} else if (internalState == State.LISTENING) {
	    if (acquireVector.evaluate(selfID, nodeID, msgVector) == Check.YOUNGER) {
		currentAcquireCounter--;
		if (currentAcquireCounter == 0) {
		    internalState = State.LAZY;
		    resourceAcquired.push(currentResourceID);
		    notifyAll();
		}
	    }
	}
    }

    public synchronized void negate(int ResourceID, int NodeID, VectorClock vector) {
	this.vector.merge(vector);
	this.vector.increment(selfID);

	if (internalState == State.WAIT) {

	} else if (internalState == State.LAZY) {
	    logger.debug("[" + selfID + "] Error Negate receive while in Lazy ");
	} else if (internalState == State.LISTENING) {
	    internalState = State.WAIT;
	}
    }

    public synchronized void notifyMsg(int ResourceID, int NodeID, VectorClock vector) {
	this.vector.merge(vector);
	this.vector.increment(selfID);

	try {
	    if (internalState == State.WAIT) {
		if (currentResourceID == ResourceID) {
		    this.setupListening(ResourceID);
		}
	    } else if (internalState == State.LAZY) {

	    } else if (internalState == State.LISTENING) {

	    }
	} catch (JMSException e) {
	    throw new RuntimeException();
	} catch (InterruptedException e) {
	    throw new RuntimeException();
	}
    }

    public VectorClock getCurrentClock() {
	return vector;
    }

    public VectorClock getAcquireClock() {
	return acquireVector;
    }

    private boolean denyIfHoldenResource(int resourceID, int nodeID) throws JMSException {
	for (int r : resourceAcquired) {
	    if (resourceID == r) {
		this.sendMessage(Protocol.NEGATE, nodeID, resourceID);
		return true;
	    }
	}
	this.sendMessage(Protocol.ACKNOWLEDGE, nodeID, resourceID);
	return false;
    }

    private void setupListening(int resourceID) throws JMSException, InterruptedException { 
	acquireVector.merge(vector);
	// TODO vincolo sulla condizione del proprio timestamp
	currentAcquireCounter = nodeNumber - 1;
	currentResourceID = resourceID;
	internalState = State.LISTENING;
	// resourceAcquired.push(resourceID);
	this.sendMessage(Protocol.REQUEST, -1, resourceID);
    }

    private void sendMessage(Enum<Protocol> type, int remoteNodeID, int resourceID) {
	vector.increment(selfID);
	try {
	    publisher.sendMessage(type, remoteNodeID, resourceID, vector);
	} catch (JMSException e) {
	    e.printStackTrace();
	}
    }

    private void releaseAcquiredResources() {
	if (internalState == State.LAZY) {
	    for (int r : resourceAcquired) {
		this.sendMessage(Protocol.NOTIFY, -1, r);
	    }
	}
	resourceAcquired.clear();
    }
}
