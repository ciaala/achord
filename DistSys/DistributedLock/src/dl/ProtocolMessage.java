package dl;
import java.io.Serializable;


public class ProtocolMessage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4115684449585610953L;

	private Enum<Protocol> type;
	/** L'origine del messaggio */
	private int sourceID;
	/** Il destinatario del messaggio */
	private int targetID;
	private int resourceID;
	private VectorClock vector;
	public ProtocolMessage(Enum<Protocol> type, int selfNodeID,int remoteNodeID, int resourceID, VectorClock vector) {
		this.type=type;
		this.sourceID=selfNodeID;
		this.targetID=remoteNodeID;
		this.resourceID=resourceID;
		this.vector=vector;
	}

	public Enum<Protocol> getType() {
		return type;
	}
	public int getSourceID() {
		return sourceID;
	}
	public int getTargetID() {
		return targetID;
	}
	public int getResourceID() {
		return resourceID;
	}
	public VectorClock getVector() {
		return vector;
	}
	@Override
	public String toString() {
	    return "{" + type + "} from: "+ sourceID + ", to: " + targetID + ", resource: <"+ resourceID+"> #"+vector;
	}
}