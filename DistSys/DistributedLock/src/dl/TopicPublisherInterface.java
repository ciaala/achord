package dl;
import javax.jms.JMSException;


public interface TopicPublisherInterface {

	public void sendMessage(Enum<Protocol> type,int remoteNodeID, int resourceID,VectorClock vector) throws JMSException;
	
	
}
 