package dl;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ActiveMQSubscriber extends Thread implements TopicSubscriberInterface {
    private Logger logger = Logger.getLogger("dl");

    private boolean activeFlag;
    private TopicSubscriber topicSubscriber;
    private TopicConnection topicConn;
    private StateMachine sm;
    private int selfID;

    public ActiveMQSubscriber(int selfID, StateMachine sm) throws NamingException, JMSException {
	activeFlag = true;
	this.sm = sm;
	this.selfID = selfID;
	InitialContext ctx = new InitialContext();
	Topic topic = (Topic) ctx.lookup("MyTopic");
	TopicConnectionFactory connFactory = (TopicConnectionFactory) ctx.lookup("ConnectionFactory");
	topicConn = connFactory.createTopicConnection();
	TopicSession topicSession = topicConn.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
	topicSubscriber = topicSession.createSubscriber(topic);

    }

    @Override
    public void run() {
	logger.debug("[" + selfID + "] ActiveMQ Subscriber Startup");
	try {
	    topicConn.start();
	    ObjectMessage message;
	    // receive a message
	    while (activeFlag) {
		message = (ObjectMessage) topicSubscriber.receive();
		ProtocolMessage msg = (ProtocolMessage) message.getObject();
		/** DEBUG */

		/* Sender */
		int sourceID = msg.getSourceID();
		if (sourceID != selfID) {
		    printMessage(msg);
		    Enum<Protocol> type = msg.getType();
		    int resourceID = msg.getResourceID();
		    /* Receiver */
		    int targetID = msg.getTargetID();
		    VectorClock receivedVector = msg.getVector();
		    
		    if (type == Protocol.ACKNOWLEDGE && targetID == selfID) {
			sm.acknowledge(resourceID, sourceID, receivedVector);
		    } else if (type == Protocol.NEGATE && targetID == selfID) {
			sm.negate(resourceID, sourceID, receivedVector);
		    } else if (type == Protocol.NOTIFY) {
			sm.notifyMsg(resourceID, sourceID, receivedVector);
		    } else if (type == Protocol.REQUEST) {
			sm.request(resourceID, sourceID, receivedVector);
		    }
		}
		// print the message
	    }
	    // close the queue connection
	    topicConn.close();
	} catch (JMSException e) {
	    throw new RuntimeException();
	}
    }

    private void printMessage(ProtocolMessage msg) {
	logger.debug("[" + selfID + "] RECV" + msg + " @"+sm.getCurrentClock());

    }

}
