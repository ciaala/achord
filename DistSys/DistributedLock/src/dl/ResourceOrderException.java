package dl;

import java.util.Collection;

public class ResourceOrderException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -893088307946005262L;
    private String ourMessage = null;
    public ResourceOrderException() {
    }

    public ResourceOrderException(String arg0) {
	super(arg0);
    }

    public ResourceOrderException(Throwable arg0) {
	super(arg0);
    }

    public ResourceOrderException(String arg0, Throwable arg1) {
	super(arg0, arg1);
    }
    public ResourceOrderException(int resource, Collection<Integer> acquired)
    {
	super();
	StringBuilder sb = new StringBuilder(); 
	sb.append("Error resource "+ resource + " is not in order {");
	for (int r : acquired) {
	    sb.append(r);
	    sb.append(",");
	}
	sb.setCharAt(sb.length()-1,'}' );
	this.ourMessage = sb.toString();
    }
    @Override
    public String getMessage() {
	if ( ourMessage == null) {
	    return super.getMessage();
	} else {
	    return ourMessage;
	}
    }
}
