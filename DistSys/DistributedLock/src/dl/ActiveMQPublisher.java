package dl;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

public class ActiveMQPublisher implements TopicPublisherInterface {
    private Logger logger = Logger.getLogger("dl");

    private TopicSession topicSession;
    private TopicPublisher topicPublisher;
    private TopicConnection topicConn;
    private int selfID;

    public ActiveMQPublisher(int selfID) throws NamingException, JMSException {
	this.selfID = selfID;
	InitialContext ctx = new InitialContext();
	// lookup the queue object
	Topic topic = (Topic) ctx.lookup("MyTopic");
	// lookup the queue connection factory
	TopicConnectionFactory connFactory = (TopicConnectionFactory) ctx.lookup("ConnectionFactory");
	// create a queue connection
	topicConn = connFactory.createTopicConnection();
	// create a queue session
	topicSession = topicConn.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
	// create a queue sender
	topicPublisher = topicSession.createPublisher(topic);
	topicPublisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
    }

    public void sendMessage(Enum<Protocol> type, int remoteNodeID, int resourceID, VectorClock vector)
	    throws JMSException {
	ObjectMessage message;
	ProtocolMessage msg = new ProtocolMessage(type, selfID, remoteNodeID, resourceID, vector);
	message = topicSession.createObjectMessage(msg);
	topicPublisher.publish(message);
	logger.debug( "["+selfID+"] SENT" + msg);
    }

    public void finish() throws JMSException {
	topicConn.close();
    }

}
