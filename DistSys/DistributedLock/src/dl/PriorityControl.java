package dl;

public class PriorityControl {
	private int selfID;
	public PriorityControl(int selfID) {
		this.selfID=selfID;
	}
	/**
	 * Vince il nodo con valore più alto
	 * @param nodeID
	 * @return
	 */
	public boolean isWinner(int nodeID)
	{
		return nodeID>selfID;
	}
}
