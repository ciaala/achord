package dl;

import java.io.Serializable;
import java.util.HashMap;

public class VectorClock implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4399626912927392018L;
    private HashMap<Integer, Integer> vector;

    private Integer getValue(int key) {
	return vector.get(key);
    }

    public VectorClock(int Nodes) {
	vector = new HashMap<Integer, Integer>(Nodes);
	for (int i = 0; i < Nodes; i++) {
	    vector.put(i, 0);
	}
    }

    public void merge(VectorClock v) {
	for (Integer node : vector.keySet()) {
	    Integer timeStamp = v.getValue(node);
	    if (timeStamp != null) {
		if (timeStamp > vector.get(node))
		    vector.put(node, timeStamp);
	    }
	}
    }

    public int increment(Integer self) {
	int newValue = vector.get(self) + 1;
	vector.put(self, newValue);
	return newValue;
    }

    public Enum<Check> evaluate(int selfID, int receivedID, VectorClock receivedVector) {
	int remoteSelf, localSelf;
//	int localReceived, remoteReceived;
	remoteSelf = receivedVector.getValue(selfID);
//	remoteReceived = receivedVector.getValue(receivedID);
	localSelf = vector.get(selfID);
//	localReceived = vector.get(receivedID);
	
	
	/** 
	 * 
	 *  L'host remoto mi risponde dopo aver
	 *  visto messaggi successivi al mio acquiring 
	 *  per l'order FIFO di JMS la richiesta 
	 *  e' successiva
	 *  
	 *  Sto verificando che la richiesta remota sia partita dopo che l'host ha ricevuto la mia richiesta
	 *  
	 *  REmoto tenta di acquisire mentre io sono ancora in listening
	 *  
	 */
	if (remoteSelf > localSelf) {
	    return Check.YOUNGER;
	} 

	/**
	 * 
	 * 
	 * 
	 */
	
	else if (remoteSelf <= localSelf) {
	    // TODO Check the other nodes Vector information
	    return Check.CONCURRENT;
	}
	/**
	*
	*
	*/
	
//	else if (remoteSelf < localSelf) {
//	    return Check.OLDER;
//	    
//	    
//	} 
	throw new RuntimeException();
    }
    @Override
    public String toString() {
	StringBuilder result = new StringBuilder();
	for ( int key: vector.keySet()) {
	    result.append(" " +key  + "=" + vector.get(key));
	}
	return result.toString();
    }
}
