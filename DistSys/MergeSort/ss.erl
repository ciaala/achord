-module(ss).
-export([run/1,step/3]).


clog(Log,Message)->
	clog(Log,Message,[]),
	void.

clog(Log,Message,Data)->
	ok=io:fwrite(Log,Message,Data),
	ok=io:fwrite(Message,Data),
	void.


run(Times)->
	erlang:garbage_collect(),
	Size=(1000),
	Array=dms:create_array(Size),
	{ok,Log}=file:open("dms.log",write),
	
	clog(Log,"Run Started ~n"),
	{Elapsed,_}=timer:tc(step,[Array,Times,Log]),

	clog(Log,"Run Completed ~p~n",[Elapsed]),

	file:close(Log).
step(Array,Times,Log) when Times==1 ->
	clog(Log,"[~p]", Times ),
	dms:main(Array,[]),
	void;

step(Array,Times,Log)->
	clog(Log,"[~p]", Times ),
	dms:main(Array,[]),
	ok=timer:sleep(35),
	step(Array,Times-1,Log).

