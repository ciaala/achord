-module(b).
-export([prepareWorkers/0,prepareWorkers/1]).

%
% Riceve due liste di elementi, ripiega la  seconda sulla prima
% [{W1,Queue1}|L1]
%	{W1, Queue1} primo elemento della prima lista
%		W1 Resto della tupla
%		Queue1 la coda degli elementi accodati a questo nodo
%	L1 resto della lista
% [{W2,Queue2}|L2]
%	{W2, Queue2} primo elemento della seconda lista
%		W2 Resto della tupla
%		Queue2 la coda degli elementi accodati a questo nodo
%	L2 resto della lista
% Restituisce la Lista costruita dalla chiamata ricorsiva
% Ricorsivamente chiama 
%

enque( Ret, [], [])->
	io:format("Ret ~p~n",[Ret]),
	Ret;
enque( [], [], [L3])->
	io:format("enque1 ~p~n",[{[],[],L3}]),
	L3;
enque( [],[W2],L3)->
	io:format("enque2 ~p~n",[{[],W2,L3}]),
	splitReverse([W2,L3]);
enque( [W1],[W2],[L3]) ->
	io:format("enque3 ~p~n",[{W1,W2,L3}]),
	splitReverse([enqueueWorker(W1,W2),L3]);
enque( [W1|L1],[W2|L2],[]) ->
	io:format("enque4 ~p~n",[{W1,W2,[]}]),
	R2 = enqueueWorker(W1,W2 ),
	enque( L1, L2, [R2]);	
enque( [W1|L1],[W2|L2],L3) ->
	io:format("enque5 ~p~n",[{W1,W2,L3}]),
	R2 = enqueueWorker(W1,W2 ),
	enque( L1, L2, [R2,L3]).

enqueueWorker({H1,P1,Q1},{H2,P2,[]}) ->
	{H2,P2,[{H1,P1,Q1,H2}]};
enqueueWorker({H1,P1,Q1},{H2,P2,Q2}) ->
	{H2,P2,[{H1,P1,Q1,H2}|Q2]}.

splitReverse(Workers) ->
	Length = length(Workers),
	{List1, List2} = lists:split(round(Length/2),Workers),
	enque(List1,lists:reverse(List2),[]).

addEmptyQueue( {Host, Power})->
	io:format("addEmptyQueue ~p~n",[{Host,Power}]),
	{Host,Power,[]}.
	
initialiseWorkers(Workers)->
	lists:map(fun addEmptyQueue/1,Workers ).
	
prepareWorkers()->
	Hosts = [{wobbo@tofu,8},{wobbo@radice,4},{wobbo@soya,4},{wobbo@polline,2}],
	prepareWorkers(lists:reverse(Hosts)).
	
prepareWorkers(Workers)->
	splitReverse(initialiseWorkers(Workers)).

	
