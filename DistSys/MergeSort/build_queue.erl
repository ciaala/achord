-module(build_queue).
-export([build_queue/1]).





%Metodo che permette la costruzione a partire dalla lista di workers di una coda di merge
%PARAMS: Workers = Lista di workers.
%	 Ret	 = Lista ausiliaria per contenere il valore di ritorno.	
%RETURN: Lista di workers con coda di merge vuota

add_queue([{Worker,Power}],Ret)->
	[{Worker,Power,[],0}|Ret];
add_queue([{Worker,Power}|Workers],Ret)->
	add_queue(Workers,[{Worker,Power,[],0}|Ret]).


%Metodo che effettua il merging tra due liste accoppiando l'elemento in posizione iesima al corrispettivo dell'altra lista
%PARAMS: Half1    = Lista 1.
%	 Half2 	  = Lista 2.
%	 Worker1,Power1 = Elemento della lista 1
%	 Worker2,Power2 = Elemento della lista 2
%	 Queue1   = Coda dell'elemento 1
%	 Queue2   = Coda dell'elemento 2	
%	 Receiver = Lista ausiliari in cui vengono messi i receiver del passo iesimo della merge
%	 Sender   = Lista ausiliari in cui vengono messi i sender del passo iesimo della merge
%RETURN: Lista di workers con coda di merge

%Step finale tra liste di pari lunghezza
merge([{Worker1,Power1,Queue1,_}],[{Worker2,Power2,Queue2,_}],Receiver,Sender)->
	{[{Worker1,Power1,Queue1++[{Worker2,Power2,Queue2,Worker1}],0}|Receiver],[{Worker2,Power2,Queue2,Worker1,Power1}|Sender]};
%Step ausiliario per liste di lunghezza differente
merge({Worker1,Power1,Queue1,_},[{Worker2,Power2,Queue2,_}],Receiver,Sender)->
	{[{Worker1,Power1,Queue1++[{Worker2,Power2,Queue2,Worker1}],0}|Receiver],[{Worker2,Power2,Queue2,Worker1,Power1}|Sender]};
%Step finale tra liste di lunghezza differente
merge([{Worker1,Power1,Queue1,_}],[{Worker2,Power2,Queue2,_}|Half2],Receiver,Sender)->
	merge({Worker1,Power1,Queue1++[{Worker2,Power2,Queue2,Worker1}],0},Half2,Receiver,[{Worker2,Power2,Queue2,Worker1,Power1}|Sender]);
%Step iniziale
merge([{Worker1,Power1,Queue1,_}|Half1],[{Worker2,Power2,Queue2,_}|Half2],Receiver,Sender)->
	merge(Half1,Half2,[{Worker1,Power1,Queue1++[{Worker2,Power2,Queue2,Worker1}],0}|Receiver],[{Worker2,Power2,Queue2,Worker1,Power1}|Sender]).

%Metodo che permette la generazione della coda di merge
%PARAMS: Workers = Lista di workers con annessa coda di merge.
%	 Ret	 = Lista ausiliaria per contenere il valore di ritorno.	
%RETURN: Lista di workers con coda di merge

%Step finale della generazione della coda
build_tree([Worker],Ret) ->
	[Worker]++Ret;
%Merge delle due liste accoppiando i primi elementi di ognuna
build_tree({Half1,Half2},Ret) ->
	{Receiver,Sender}=merge(Half1,Half2,[],[]),
	build_tree(lists:reverse(Receiver),lists:reverse(Sender)++Ret);
%Step iniziale della generazione della coda
build_tree(Workers,Ret) ->
	build_tree(lists:split(trunc(length(Workers)/2),Workers),Ret).


%Metodo attraverso il quale viene generata la coda di merge tra workers
%PARAMS: Workers = Lista di workers.
%RETURN: Lista di workers con coda di merge
build_queue(Workers) ->
%	build_tree(add_queue(Workers,[]),[]).
	[Ret | _]=build_tree(add_queue(Workers,[]),[]),
	Ret.
