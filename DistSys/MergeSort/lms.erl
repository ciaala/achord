-module(lms).
-export([remoteMerger/3,remoteSorter/2,remoteRegister/0,hostid/0,log/1,log/2]).
hostid()->
	{ok,[{MyHostname,_}]} = net_adm:names(),
	list_to_atom(lists:concat([ MyHostname,"@",net_adm:localhost() ])).
log(Message)->
	%log(Message,[]),
	void.
log(Message,Data) ->
	%ok = io:fwrite("<~s> ",[hostid()]),
	%ok = io:fwrite(Message,Data),
	void.
	 
checkMerger(Element) ->
	if Element == merger ->
		unregister(merger),
		true;
	true->
		false
	end.

remoteRegister() ->
	lists:any(fun checkMerger/1, registered() ),
	register(merger, self()).
	
remoteMerger(Times,Receiver,Last) ->
	remoteRegister(),
	log("RemoteMerger Ready ~p~n",[{Times,Receiver}]),
	receive
	{remote,Array} ->
		log("RemoteMerger received from remote host ~n"),
		remoteMerger(Array,Times-1,Receiver,Last);
	{local,Array} ->
		log("RemoteMerger received from local host~p ~n",[[Receiver,hostid()]]),
		remoteMerger(Array,Times-1,Receiver,Last)
	after
	100000 ->
		log("RemoteMerger Timeout~n")
	end.

%	
% Used by the remote machine with just one thread of work assigned.
%
%remoteMerger(Array,0,Receiver) ->
%	log("RemoteMerger ~p ~n",[0]),
%	log("RemoteMerger sending to~p~n",[{merger,Receiver}]),
%	{merger,Receiver} ! {remote,Array};


remoteMerger(Array,0,Receiver,true) ->
	log("RemoteMerger ~p ~n",[0]),
	log("RemoteMerger sending to~p~n",[{merger,Receiver}]),
	{receiver,Receiver} ! Array;
remoteMerger(Array,0,Receiver,false) ->
	log("RemoteMerger ~p ~n",[0]),
	log("RemoteMerger sending to~p~n",[{merger,Receiver}]),
	{merger,Receiver} ! {remote,Array};

remoteMerger(Array,1,Receiver,true) ->
	log("RemoteMerger ~p ~n",[1]),
	receive
	{remote, ReceivedArray} ->
		log("RemoteMerger received from remote host~n"),
		ResultArray = localMerge(ReceivedArray,Array),
		log("RemoteMerger sending to~p~n",[{hostid(),merger,Receiver}]),
		{receiver,Receiver} ! ResultArray;
	{local, ReceivedArray} ->
		log("RemoteMerger received from local host~n"),
		ResultArray = localMerge(ReceivedArray,Array),
		log("RemoteMerger sending to~p~n",[{receiver,Receiver,length(ResultArray)}]),
		log("RemoteMerger Job done ~n"),
		erlang:send({receiver,Receiver}, ResultArray),
		%{receiver,Receiver} ! "done",
		log("RemoteMerger Job gg done ~n")
	after
	100000 ->
		log("RemoteMerger Timeout~n")
	end;

remoteMerger(Array,1,Receiver,false) ->
	log("RemoteMerger ~p ~n",[1]),
	receive
	{remote, ReceivedArray} ->
		log("RemoteMerger received from remote host~n"),
		ResultArray = localMerge(ReceivedArray,Array),
		log("RemoteMerger sending to~p~n",[{hostid(),merger,Receiver}]),
		{merger,Receiver} ! {remote,ResultArray};
	{local, ReceivedArray} ->
		log("RemoteMerger received from local host~n"),
		ResultArray = localMerge(ReceivedArray,Array),
		log("RemoteMerger sending to~p~n",[{hostid(),merger,Receiver}]),
		{merger,Receiver} ! {remote,ResultArray}
	after
	100000 ->
		log("RemoteMerger Timeout~n")
	end;
remoteMerger(Array,Times,Receiver,Last) ->
	log("RemoteMerger ~p ~n",[Times]),
	receive
	{remote, ReceivedArray} ->
		log("RemoteMerger received from remote host~n"),
		ResultArray = localMerge(ReceivedArray,Array),
		remoteMerger(ResultArray,Times-1,Receiver,Last);
	{local, ReceivedArray} ->
		log("RemoteMerger received from local host~n"),
		ResultArray = localMerge(ReceivedArray,Array),
		remoteMerger(ResultArray,Times-1,Receiver,Last)
	after
	100000 ->
		log("RemoteMerger Timeout~n")
	end.

	
localMerge(Array1,Array2) ->
	log("==Merger== Running ~n"),
	merge(fun lte/2, Array1, Array2).

%localMerge(Array1,Array2) ->
%	log("==Merger== Running ~n"),
%	merger ! {local,merge(fun lte/2, Array1, Array2)}.

%
%
%
remoteSorter(Host,Array) ->
%	log("==Sorter== Running ~n"),
	Sorted = msort_lte(Array),
	merger ! {local,Sorted}.

	
%
% MERGE SORT LOCALE
%
	
split(Ls) ->
    split(Ls, Ls, []).

split([], Ls1, Ls2) ->
    {Ls2 , Ls1};

split([_], Ls1, Ls2) ->
    {Ls2 , Ls1};

split([_,_|TT], [Ls1_H | Ls1_T], Ls2) ->
    split(TT, Ls1_T, [Ls1_H | Ls2]).

%L = [random:uniform(1000000) || N <- lists:seq(1, 1000000)].
%timer:tc(mergesort,parallel_msort,[[{"c0",1},{"c1",1},{"c2",1},{"c3",1},{"c4",1},{"c5",1},{"c6",1},{"c7",1}],L]).


merge(_, [], Ls2) ->
    Ls2;
merge(_, Ls1, []) ->
    Ls1;
merge(Rel, [H1|T1], [H2|T2]) ->
    case Rel(H1, H2) of
	true ->
	    [H1 | merge(Rel, T1, [H2|T2])];
	false ->
	    [H2 | merge(Rel, [H1|T1], T2)]
    end.


msort(_, []) ->
    [];
msort(_, [H]) ->
    [H];
msort(Rel, Ls) ->
    {Half1 , Half2} = split(Ls),
    merge(Rel, msort(Rel, Half1), msort(Rel, Half2)).

lte(X, Y) ->
    (X < Y) or (X == Y).

gte(X, Y) ->
    (X > Y) or (X == Y).

msort_lte(Ls) ->
    msort(fun lte/2, Ls).
	
msort_gte(Ls) ->
    msort(fun gte/2,Ls).

