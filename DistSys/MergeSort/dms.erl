-module(dms).
-export([main/2,create_array/1,getChunkSizes/2,getWorkers/1,resultReceiver/0,ceiling/1,loopSpawnRemoteSorters/4]).

hostid()->
	{ok,[{MyHostname,_}]} = net_adm:names(),
	list_to_atom(lists:concat([ MyHostname,"@",net_adm:localhost() ])).

getWorkers([])->
	%Hosts = [{dwalin@dwalin,8},{wobbo@soya,2}],{wobbo@polline,2}
	%Hosts = [{wobbo@tofu,8},{wobbo@radice,4},{wobbo@soya,4}],
	%Hosts = [{tofu@tofu,16}, {farro@farro,6},{soya@soya,2},{foglia@foglia,4},{pd00081@PD00081,2},{polline@polline,1}],
	Hosts = [{tofu@tofu,16}, {farro@farro,6},{soya@soya,2},{foglia@foglia,4},{polline@polline,1}],
	{Hosts,bwt:build_queue(lists:reverse(Hosts))};
getWorkers([[]])->
	%Hosts = [{dwalin@dwalin,8},{wobbo@soya,2}],{wobbo@polline,2}
	%Hosts = lists:reverse([{download@durin,1},{simone@nori,4}]),
	%Hosts = lists:reverse([{download@durin,1},{simone@nori,4},{simone@dwalin,16}]),
	Hosts = lists:reverse([{wobbo@soya,2},{simone@dwalin,8}]),
	{Hosts,bwt:build_queue(lists:reverse(Hosts))};
getWorkers(Hosts)->
	{Hosts,bwt:build_queue(Hosts)}.

% 
resultReceiver() ->
	register(receiver,self()),
%TODO fare un controllo di mittente, usar la lists_toAtom per convertire l'host del merger-finale in atomo identificativo.
	receive
		Array ->
			io:fwrite("Array Received ~p~n",[length(Array)])
	after 
		1500000 ->
			lms:log("ResultServer Timeout ~n")
	end.
	
sumPower({_,Power}, Total) ->
	Total + Power.
ceiling(X) ->
	T = erlang:trunc(X),
	case (X-T) of
		Neg when Neg < 0 -> T;
		Pos when Pos > 0 -> T+1;
		_ -> T
	end.

getChunkSizes(Hosts,Length) -> 
	TotalCPU = lists:foldl( fun sumPower/2,0,Hosts),
	ceiling( Length/TotalCPU).
	
	
%
% Hosts: Tuples of {name@Host_id, int CPUPower }
%%
% Foglia con coda vuota "4" con fratelli
%
% loopAssignJob([{W1_host, W1_power,[],W1_receiver}| Brothers],Array,Chunk) ->
% 	spawnMerger(W1_host, W1_power, length( W1_queue), W1_receiver),
% 	loopSpawnRemoteSorters(W1_host, W1_power, Chunk, Array );
% %	
% % Foglia con coda piena "7"
% %
% loopAssignJob([{W1_host, W1_power,W1_queue,W1_receiver}],Array,Chunk) ->


spawnMergerLast(W1_host, W1_power,Length, W1_receiver) ->
	lms:log("[SPAWNING MSORTER] ~p~n",[[W1_power+Length, W1_receiver]]),
	spawn( W1_host, lms,remoteMerger, [W1_power+Length, W1_receiver,true]).

spawnMerger(W1_host, W1_power,Length, W1_receiver) ->
	lms:log("[SPAWNING MSORTER] ~p~n",[[W1_power+Length, W1_receiver]]),
	spawn( W1_host, lms,remoteMerger, [W1_power+Length, W1_receiver,false]).
	

	
loopSpawnRemoteSorters( W1_host, 1, Array, Chunk) when Chunk >= length(Array)->
	spawn( W1_host, lms,remoteSorter, [W1_host,Array]),
	[];
loopSpawnRemoteSorters( W1_host, 1, Array, Chunk) ->
	{MyArray,RemainingArray} = lists:split(Chunk,Array),
	spawn( W1_host, lms,remoteSorter, [W1_host,MyArray]),
	RemainingArray;

	
loopSpawnRemoteSorters( W1_host, W1_power, Array, Chunk) ->
	{MyArray,RemainingArray} = lists:split(Chunk,Array),
	spawn( W1_host, lms,remoteSorter, [W1_host,MyArray]),
	loopSpawnRemoteSorters(W1_host,W1_power-1,RemainingArray,Chunk).


loopAssignJob([],[],_) ->
	lms:log("All workers spawned ~n"),
	ok;
%% LONELY SON
loopAssignJob([],Array,_) ->
	lms:log("All Queue workers spawned ~n"),
	Array;
	
%% LAST BROTHERS
loopAssignJob([{W1_host, W1_power,W1_queue,W1_receiver}],Array,Chunk) when Chunk*W1_power >= length(Array) ->
	lms:log("[NO SPLIT] Assigning jobs to ~p ~n",[[W1_host,length(Array)]] ),
	spawnMerger(W1_host, W1_power, length( W1_queue), W1_receiver),
	RemoteArray = Array,
	RemainingArray1 = [], 
	spawn(dms,loopSpawnRemoteSorters,[W1_host, W1_power, RemoteArray, Chunk]),
	loopAssignJob(W1_queue, RemainingArray1, Chunk);
loopAssignJob([{W1_host, W1_power,W1_queue,W1_receiver}],Array,Chunk) ->
	lms:log("Assigning jobs to ~p ~n",[[W1_host,length(Array)]] ),
	spawnMerger(W1_host, W1_power, length( W1_queue), W1_receiver),
	{RemoteArray,RemainingArray1} = lists:split(Chunk*W1_power,Array),
	spawn(dms,loopSpawnRemoteSorters,[W1_host, W1_power, RemoteArray, Chunk]),
	loopAssignJob(W1_queue, RemainingArray1, Chunk);
	
%% BROTHERS AND SONS
loopAssignJob([{W1_host, W1_power,W1_queue,W1_receiver}| BrothersQueues],Array,Chunk) when Chunk*W1_power >= length(Array) ->
	lms:log("[NO SPLIT] Assigning jobs to ~p ~n",[[W1_host,length(Array)]] ),
	spawnMerger(W1_host, W1_power, length( W1_queue), W1_receiver),
	RemoteArray = Array,
	RemainingArray1 = [], 
	spawn(dms, loopSpawnRemoteSorters,[W1_host, W1_power, RemoteArray, Chunk]),
	RemainingArray2 = loopAssignJob(W1_queue, RemainingArray1, Chunk),
	loopAssignJob(BrothersQueues, RemainingArray2, Chunk);
loopAssignJob([{W1_host, W1_power,W1_queue,W1_receiver}| BrothersQueues],Array,Chunk) ->
	lms:log("Assigning jobs to ~p ~n",[[W1_host,length(Array)]] ),
	spawnMerger(W1_host, W1_power, length( W1_queue), W1_receiver),
	{RemoteArray,RemainingArray1} = lists:split(Chunk*W1_power,Array),
	spawn(dms, loopSpawnRemoteSorters,[W1_host, W1_power, RemoteArray, Chunk]),
	RemainingArray2 = loopAssignJob(W1_queue, RemainingArray1, Chunk),
	loopAssignJob(BrothersQueues, RemainingArray2, Chunk).
	
%% Top Element
loopAssignJob({W1_host, W1_power,W1_queue,0},Array,Chunk, Receiver) when Chunk*W1_power >= length(Array)->
	lms:log("[NO SPLIT] Assigning jobs Last to ~p ~n",[[W1_host,length(Array)]] ),
	spawnMergerLast(W1_host, W1_power, length( W1_queue), Receiver),
	RemoteArray = Array,
	RemainingArray = [], 
	spawn( dms,loopSpawnRemoteSorters,[W1_host, W1_power, RemoteArray, Chunk]),
	loopAssignJob(W1_queue, RemainingArray, Chunk);
loopAssignJob({W1_host, W1_power,W1_queue,0},Array,Chunk, Receiver) ->
	lms:log("Assigning jobs Last to ~p ~n",[[W1_host,length(Array)]] ),
	spawnMergerLast(W1_host, W1_power, length( W1_queue), Receiver),
	{RemoteArray,RemainingArray} = lists:split(Chunk*W1_power,Array), 
	spawn( dms,loopSpawnRemoteSorters,[W1_host, W1_power, RemoteArray, Chunk]),
	loopAssignJob(W1_queue, RemainingArray, Chunk).
	
main(Array,Hosts)->
	%TODO Al momento il nodo richiedente la mergesort non ha priorita' rispetto agli altri nell'assegnamento delle operazioni di merge.
	{H,Workers} = getWorkers(Hosts),
	spawn( dms,resultReceiver,[]),
	Chunk = getChunkSizes(H,length(Array)),
	lms:log("Workers,Chunk  ~p ~n",[[Workers,Chunk]] ),
	{ok,[[MyHostname]]} = init:get_argument(sname),
	loopAssignJob(Workers,Array,Chunk,hostid()),
	erlang:garbage_collect().
create_array(Length)->
	[random:uniform() || N <- lists:seq(1, Length)].
