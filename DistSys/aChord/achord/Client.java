/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package achord;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author crypt,simone
 */
public class Client {
	private LocalChord localManager;
	private String selfHost;
	private String selfID;

	public Client(String selfHost, String selfID) throws RemoteException,
			MalformedURLException {
		this.selfHost = selfHost;
		this.selfID = selfID;

		localManager = new LocalChord(selfHost, selfID);

	}

	public void join(String remoteHost, String remoteID)
			throws RemoteException, NotBoundException {
		localManager.join(remoteHost, remoteID);
	}

	/**
	 * 
	 * @param args
	 *            args[0] = selfHost args[1] = selfID args[2] = remoteHost
	 *            args[3] = remoteID
	 */
	public static void main(String[] args) {
		System.out.println("Starting Client");
		try {
			if (args.length >= 2) {
				Client c = new Client(args[0], args[1]);
				if (args.length == 4) {
					c.join(args[2], args[3]);
					System.out.println("Network Join");
				} else {
					System.out.println("Network Startup");
				}
			}
		} catch (RemoteException ex) {
			Logger.getLogger(Client.class.getName())
					.log(Level.SEVERE, null, ex);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
