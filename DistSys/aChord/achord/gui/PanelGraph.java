package achord.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.RepaintManager;

import achord.Finger;
import achord.NodeDescriptor;

@SuppressWarnings("serial")
public class PanelGraph extends JComponent implements MouseListener {

	final Color fixedColor = Color.red;

	final Color comunicationColor = Color.blue;

	final Color selectColor = new Color(128, 198, 128);

	final Color putColor = new Color(88, 158, 88);

	final Color edgeColor = Color.black;

	final Color nodeColor = new Color(250, 220, 100);

	private Color actionColor = null;

	private int nnodes;

	private ArrayList<Node> nodes = new ArrayList<Node>();

	private ArrayList<Edge> edges = new ArrayList<Edge>();

	private int r = 0;

	private Node pick;

	private Image offscreen;

	private Dimension offscreensize;

	private Graphics offgraphics;

	// private String stabilizedString = "";

	private Controller controller;

	// private boolean isStabilized;

	private Edge comunicationEdge;

	private boolean paint = false;

	public PanelGraph() {
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		setBackground(new Color(192, 192, 192));
		addMouseListener(this);
		setOpaque(true);
		RepaintManager.currentManager(this).setDoubleBufferingEnabled(true);
		setDoubleBuffered(true);
		nnodes = 0;
	}

	public Node findNode(String lbl) {
		for (int i = 0; i < nnodes; i++) {
			if (nodes.get(i).getLbl().equals(lbl)) {
				return nodes.get(i);
			}
		}

		return null;
	}

	public int addNode(String lbl, double angle, String value) {
		Node n = new Node();

		n.setX((r / 2) * Math.cos(angle * 2.0 * Math.PI / 360)
				+ (getSize().width / 2) - 2);
		n.setY((r / 2) * Math.sin(angle * 2.0 * Math.PI / 360)
				+ (getSize().height / 2));

		n.setNodeIdentifier(value);
		n.setLbl(lbl);

		nodes.add(n);

		return nnodes++;
	}

	public void addEdge(String from, String to, String successor) {
		Edge e = new Edge();

		e.setFrom(findNode(from));
		e.setTo(findNode(to));
		e.setSuccessor(successor);

		getEdges().add(e);
	}

	public void deleteTrace() {
		edges.clear();
		comunicationEdge = null;
		paint = false;
	}

	public void makeRing(List<Finger> fingers) {
		reset();

		double angle = 0;
		double dr = 360 / fingers.size();

		for(Finger f : fingers) {
			NodeDescriptor nd = f.getHandler();
			addNode("" + nd.getID(), angle, nd.getHost());
			angle += dr;
		}
		repaint();
	}

	public void makeRing() {
		int numOfNodes = nodes.size();

		if (numOfNodes == 0) {
		//	repaint();
			return;
		}

		double angle = 0;
		double dr = 360 / numOfNodes;

		for (int i = 0; i < numOfNodes; i++) {
			Node n = nodes.get(i);

			n.setX((r / 2) * Math.cos(angle * 2.0 * Math.PI / 360)
					+ (getSize().width / 2) - 2);
			n.setY((r / 2) * Math.sin(angle * 2.0 * Math.PI / 360)
					+ (getSize().height / 2));

			angle += dr;
		}

		//repaint();
	}
	@Override
	public Dimension getPreferredSize() {
		Dimension result = new Dimension(100,100); 
		return result;
	}
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		Graphics2D g2d = (Graphics2D) g;

		Dimension d = getSize();

		r = Math.min(d.height, d.width) - 100;

		makeRing();

		// Erases the graphic panel.
		if ((offscreen == null) || (d.width != offscreensize.width)
				|| (d.height != offscreensize.height)) {
			offscreen = createImage(d.width, d.height);
			offscreensize = d;

			if (offgraphics != null) {
				offgraphics.dispose();
			}

			offgraphics = offscreen.getGraphics();
			offgraphics.setFont(getFont());
		}

		offgraphics.setColor(getBackground());

		offgraphics.fillRect(0, 0, d.width, d.height);

		offgraphics.setColor(edgeColor);
		g2d.setStroke(new BasicStroke(1f));
		offgraphics.drawOval(((d.width - r) / 2), ((d.height - r) / 2), r, r);

		FontMetrics fm = offgraphics.getFontMetrics();

		if (paint) {

			for (int i = 0; i < edges.size(); i++) {
				paintEdge((Graphics2D) offgraphics, edges.get(i), actionColor);
			}

			makeComunicationEdge();

			if (comunicationEdge != null)
				paintEdge((Graphics2D) offgraphics, comunicationEdge,
						comunicationColor);

		}

		for (int i = 0; i < nnodes; i++) {
			paintNode((Graphics2D) offgraphics, nodes.get(i), fm);
		}
		g.drawImage(offscreen, 0, 0, null);

	}

	private void makeComunicationEdge() {
		if (edges.size() >= 2 && comunicationEdge == null) {
			Edge ed = edges.get(edges.size() - 2);

			comunicationEdge = new Edge();

			comunicationEdge.setFrom(pick);
			comunicationEdge.setTo(findNode(ed.getSuccessor()));
			comunicationEdge.setSuccessor(ed.getSuccessor());
		} else {
			if (edges.size() == 0 && comunicationEdge == null && paint) {
				comunicationEdge = new Edge();

				comunicationEdge.setFrom(pick);

				int suc = getNode(pick.getLbl());

				Node successor = null;

				if (suc != -1) {
					if ((suc + 1) == nodes.size()) {
						successor = nodes.get(0);
					} else {
						successor = nodes.get(suc + 1);
					}
				}

				comunicationEdge.setTo(successor);
				comunicationEdge.setSuccessor(successor.getLbl());
			}
		}
	}

	private void paintNode(Graphics2D g, Node n, FontMetrics fm) {

		int x = (int) n.getX();
		int y = (int) n.getY();

		if (n == pick) {
			g.setColor(selectColor);
		} else {
			g.setColor(nodeColor);
		}
		g.setStroke(new BasicStroke(1f));
		// calcula el ancho y la altura de cada rectangulo
		int w = fm.stringWidth(n.getLbl()) + 10;
		int h = fm.getHeight() + 4;

		// dibuja el rectangulo de color con centro en x,y y el ancho y altura
		// especificadas
		g.fillRect(x - w / 2, y - h / 2, w, h);

		g.setColor(Color.black);

		// dibuja el rectangulo (borde)
		g.drawRect(x - w / 2, y - h / 2, w - 1, h - 1);

		// escribe el nombre del nodo, teniendo en cuenta la altura de la fuente
		g.drawString(n.getLbl(), x - (w - 10) / 2,
				(y - (h - 4) / 2) + fm.getAscent());
	}

	private void paintEdge(Graphics2D g, Edge e, Color color) {
		if (e == null) {
			return;
		}

		int x1 = (int) e.getFrom().getX();
		int y1 = (int) e.getFrom().getY();
		int x2 = (int) e.getTo().getX();
		int y2 = (int) e.getTo().getY();

		g.setColor(color);

		if (e.getFrom().getLbl().equals(e.getTo().getLbl())) {
			g.drawOval(x1, y1, 30, 30);
			return;
		}
		g.setStroke(new BasicStroke(2f));
		g.drawLine(x1, y1, x2, y2);

		g.setColor(Color.BLUE);
		g.fillOval(x1 - 3, y1 - 3, 6, 6);

	}

	private void reset() {
		nnodes = 0;
		nodes.clear();
		getEdges().clear();

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		double bestdist = 70.0;

		int x = e.getX();
		int y = e.getY();

		for (int i = 0; i < nnodes; i++) {
			Node n = nodes.get(i);
			double dist = (n.getX() - x) * (n.getX() - x) + (n.getY() - y)
					* (n.getY() - y);

			if (dist < bestdist) {
				pick = n;
				break;
			}
		}

		if (pick != null) {
			controller.changeToNode(pick.getNodeIdentifier());
		}

		//repaint();
	}

	public void setActionColor(boolean put) {
		actionColor = (put) ? putColor : fixedColor;
		paint = true;
	}

	public int getNnodes() {
		return nnodes;
	}

	public int getNode(String node) {
		for (int i = 0; i < nnodes; i++) {
			if (nodes.get(i).getLbl().equals(node)) {
				return i;
			}
		}

		return -1;

	}

	public void setPickNode(String name) {
		Node node = findNode(name);
		pick = node;
		controller.clearGraph();
	//	repaint();
	}

	public void setController(Controller controller) {
		this.controller = controller;
	}

	public Controller getController() {
		return controller;
	}

	public void setEdges(ArrayList<Edge> edges) {
		this.edges = edges;
		//repaint();
	}

	public ArrayList<Edge> getEdges() {
		return edges;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
}