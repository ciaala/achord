package achord.gui;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import achord.Finger;
import achord.FingerTable;

public class FingerModel extends AbstractTableModel {

	/**
     * 
     */
	private static final long serialVersionUID = -2074808317247794076L;
	private ArrayList<Finger> fingers;

	public FingerModel(FingerTable table) {
		fingers = table.getFingers();
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public int getRowCount() {
		return fingers.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		Finger finger = fingers.get(row);
		switch (col) {
			case 0 :
				return finger.step;
			case 1 :
				return new String(finger.start + "-" + finger.end);
			case 2 :
				return finger.handler.getHost();
			case 3 :
				return finger.handler.getInterfaceName();
			default :
				return null;
		}
	}

}
