package achord.gui;

public class Edge {
	
	private Node from;
	
	private Node to;
	
	private String successor;
	
	public Node getFrom() {
		return from;
	}
	
	public void setFrom(Node from) {
		this.from = from;
	}
	
	public Node getTo() {
		return to;
	}
	
	public void setTo(Node to) {
		this.to = to;
	}
	
	public void setSuccessor(String successor) {
		this.successor = successor;
	}
	
	public String getSuccessor() {
		return successor;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return from.getNodeIdentifier()+" "+to.getNodeIdentifier();
	}
}