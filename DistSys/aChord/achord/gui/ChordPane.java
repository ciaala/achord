package achord.gui;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;

import achord.FingerTable;
import achord.LocalChord;

public class ChordPane extends JPanel implements ActionListener {

	static double angle = 0;
	private static final long serialVersionUID = -2479654996571379008L;
	private JButton join;
	private JTextField host;
	private JTable table;
	private JTextField id;
	private PanelGraph pg;
	private LocalChord localManager;
	private JButton leave;
	private JButton selfTable;
	private JTextField key;
	private JTextField value;
	private JButton put;
	private JButton get;
	private JButton getif;
	private JButton take;
	private JButton takeif;

	private String hostname;
	private String interfaceID;
	private JFrame frame;
	private long hash = -1;

	public ChordPane(JFrame frame, String selfHost, String selfID) {
		
		super(new GridBagLayout());
		this.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		this.hostname = selfHost;
		this.interfaceID = selfID;
		this.frame = frame;
		
		try {
			localManager = new LocalChord(selfHost, selfID);
			hash = localManager.getSelfDescriptor().getHash();			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		frame.setTitle("aChord [host: "+hostname+"] [id: "+ interfaceID + "] [hash: "+hash +"]");
		setupGUI();
	}
	
	public void setupGUI() {	
		setupRowZero(this);
		JPanel joinedGroup = new JPanel(new GridBagLayout());
		joinedGroup.setBorder(BorderFactory
				.createBevelBorder(BevelBorder.LOWERED));
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 1;
		c.gridwidth = 5;
		c.weightx = 1.0;
		add(joinedGroup, c);

		setupRowOne(joinedGroup);
		setupRowTwo(joinedGroup);
		setupRowThree(this);

	}

	private void setupRowThree(JPanel owner) {
		selfTable = new JButton("My Finger Table");
		selfTable.addActionListener(this);
		selfTable.setPreferredSize(new Dimension(80, 20));

		table = new JTable();
		pg = new PanelGraph();

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.gridy = 2;

		c.gridx = 3;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		add(selfTable, c);

		c.gridy = 3;
		c.gridx = 0;
		c.gridwidth = 3;

		c.weightx = 2.0;
		c.weighty = 2.0;
		c.fill = GridBagConstraints.BOTH;
		add(pg, c);

		c.gridx = 3;
		c.gridwidth = 2;
		c.weightx = 2.0;

		c.fill = GridBagConstraints.HORIZONTAL;

		add(new JScrollPane(table), c);

	}
	/**
	 * TAKE GET
	 * 
	 * @param owner
	 */
	private void setupRowTwo(JPanel owner) {

		get = new JButton("get");
		get.addActionListener(this);
		get.setPreferredSize(new Dimension(80, 20));
		getif = new JButton("getif");
		getif.addActionListener(this);
		getif.setPreferredSize(new Dimension(80, 20));
		take = new JButton("take");
		take.addActionListener(this);
		take.setPreferredSize(new Dimension(80, 20));
		takeif = new JButton("takeif");
		takeif.addActionListener(this);
		takeif.setPreferredSize(new Dimension(80, 20));
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.gridy = 1;

		c.gridx = 0;
		owner.add(get, c);
		c.gridx++;
		owner.add(getif, c);
		c.gridx++;
		owner.add(take, c);
		c.gridx++;
		owner.add(takeif, c);
	}
	/**
	 * Put
	 */
	private void setupRowOne(JPanel owner) {
		key = new JTextField("key", 20);
		key.setPreferredSize(new Dimension(80, 20));
		value = new JTextField("value", 20);
		value.setPreferredSize(new Dimension(80, 20));

		put = new JButton("put");
		put.addActionListener(this);
		put.setPreferredSize(new Dimension(80, 20));

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.gridy = 0;

		c.gridx = 0;
		owner.add(new JLabel("Key"));
		c.gridx = 1;
		owner.add(key, c);
		c.gridx = 2;
		owner.add(new JLabel("Value"));
		c.gridx = 3;
		owner.add(value, c);
		c.gridx = 4;
		owner.add(put, c);
	}

	/**
	 * Join && Leave
	 */
	private void setupRowZero(JPanel owner) {
		host = new JTextField(hostname, 20);
		host.setPreferredSize(new Dimension(80, 20));
		id = new JTextField("ChordInterface", 20);
		id.setPreferredSize(new Dimension(80, 20));
		join = new JButton("join");
		join.addActionListener(this);
		join.setPreferredSize(new Dimension(80, 20));
		leave = new JButton("leave");
		leave.addActionListener(this);
		leave.setPreferredSize(new Dimension(80, 20));

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.gridy = 0;

		c.gridx = 0;
		owner.add(new JLabel("Host"), c);

		c.gridx = 1;
		owner.add(host, c);

		c.weightx = 0;
		c.gridx = 2;
		owner.add(new JLabel("/"), c);

		c.gridx = 3;
		owner.add(id, c);

		c.gridx = 4;
		owner.add(join, c);

		c.gridx = 4;
		owner.add(leave, c);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == join) {
			localManager.join(host.getText(), id.getText());
			this.setJoined(true);
		} else if (e.getSource() == leave) {
			localManager.leave();
			this.setJoined(false);
		} else if (e.getSource() == selfTable) {
			showFingerTable(localManager.getFingerTable());
		}
	}

	private void setJoined(boolean value) {
		join.setVisible(!value);
		leave.setVisible(value);
		host.setEnabled(!value);
		id.setEnabled(!value);
	}

	public void showFingerTable(FingerTable ft) {
		table.setModel(new FingerModel(ft));

	}

}
