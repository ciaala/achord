package achord.gui;

public class Node {
	
	private double x;
	
	private double y;
	
	private String nodeIdentifier;
	
	private String lbl;
	
	public String getLbl() {
		return lbl;
	}
	
	public void setLbl(String lbl) {
		this.lbl = lbl;
	}
	
	public double getX() {
		return x;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public double getY() {
		return y;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public void setNodeIdentifier(String nodeIdentifier) {
		this.nodeIdentifier = nodeIdentifier;
	}
	
	public String getNodeIdentifier() {
		return nodeIdentifier;
	}
}