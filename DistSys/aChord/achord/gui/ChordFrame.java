package achord.gui;
import java.net.Inet4Address;
import java.net.UnknownHostException;

import javax.swing.JFrame;


public class ChordFrame {
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     * @throws UnknownHostException 
     */
    private static void createAndShowGUI() throws UnknownHostException {
        //Create and set up the window.
    	String host = Inet4Address.getLocalHost().getHostName();
    	String id = Long.toString(System.currentTimeMillis() % 1024);
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        ChordPane newContentPane = new ChordPane(frame,host,id);
        //content panes must be opaque
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
  
                	try {
						createAndShowGUI();
					} catch (UnknownHostException e) {
						e.printStackTrace();
						System.exit(1);
					}
  
            }
        });
    }
}
