package achord;

import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;

public interface ChordStub extends Remote {

	public void put(NetworkIdentifier ni, Serializable sr)
			throws RemoteException, NotBoundException;

	public void put(HashMap<NetworkIdentifier, Serializable> res)
			throws RemoteException, NotBoundException;

	public Serializable get(NetworkIdentifier ni) throws RemoteException,
			NotBoundException;

	public Serializable getIfExist(NetworkIdentifier ni)
			throws RemoteException, NotBoundException;

	public Serializable take(NetworkIdentifier ni) throws RemoteException,
			NotBoundException;

	public Serializable takeIfExist(NetworkIdentifier ni)
			throws RemoteException, NotBoundException;

	public Neighbour join(NodeDescriptor node) throws RemoteException,
			NotBoundException;

	public NodeDescriptor getSuccessor() throws RemoteException;

	public NodeDescriptor getPredecessor() throws RemoteException;

	public NodeDescriptor findSuccessor(NetworkIdentifier ni, int hop)
			throws RemoteException, NotBoundException;
	public NodeDescriptor findSuccessor(long hash, int hop)
			throws RemoteException, NotBoundException;
	public void updateOnJoin(NodeDescriptor ni) throws RemoteException,
			NotBoundException;

	public void updateOnLeave(NodeDescriptor ni,NodeDescriptor substitute) throws RemoteException,
			NotBoundException;

	public boolean leaveOnSuccessor(NodeDescriptor node,Neighbour remotenb, HashMap<NetworkIdentifier, Serializable> resources) throws RemoteException,
			NotBoundException;

	public boolean leaveOnPredecessor(NodeDescriptor node,Neighbour remotenb) throws RemoteException,
			NotBoundException;

	public void notifyResource(Serializable sr) throws RemoteException;
}
