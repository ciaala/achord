package achord;


import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;


public class Hasher {
    public static BigInteger hashThis(String thiz,int exponent) {
	BigInteger result;
	BigInteger modulus = new BigInteger("2");
	modulus = modulus.pow(exponent);
    MessageDigest md;
	try {
		md = MessageDigest.getInstance("MD5");
	    md.update(thiz.getBytes());
		result = new BigInteger(1, md.digest());
		result = result.mod(modulus);
		return result;
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		throw new RuntimeException("Hash Error");
	}

    }
}
