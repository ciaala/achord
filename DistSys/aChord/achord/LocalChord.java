package achord;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class LocalChord implements ChordInterface {
	private ChordManager cm;

	// private NodeDescriptor selfNode;

	public LocalChord(String selfHost, String ID) throws RemoteException,
			MalformedURLException {
		this.cm = new ChordManager(selfHost, ID);
		// this.selfNode = new NodeDescriptor(selfHost, ID);
		this.cm.start();
	}

	public void join(String remoteHost, String remoteID) {
		try {
			cm.join(remoteHost, remoteID);
			System.out.println("Joined " + remoteHost + remoteID);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Join error "+e.getMessage());
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Join error "+e.getMessage());
		}

	}

	@Override
	public void put(NetworkIdentifier ni, Serializable sr) {
		// TODO Auto-generated method stub
		NodeDescriptor handler;
		try {
			handler = cm.findSuccessor(ni, 0);
			cm.getRemoteInterface(handler).put(ni, sr);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Put error "+e.getMessage());
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Put error "+e.getMessage());
		}
	}

	@Override
	public Serializable get(NetworkIdentifier ni) {
		try {
			NodeDescriptor handler = cm.findSuccessor(ni, 0);
			Serializable ret = cm.getRemoteInterface(handler).get(ni);
			if (ret == null) {
				while (cm.getNotifiedResource() == null) {
					cm.wait();
				}
				ret = cm.getNotifiedResource();
			}
			return ret;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Get error "+e.getMessage());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("get error "+e.getMessage());
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("get error "+e.getMessage());
		}
	}

	@Override
	public Serializable getIfExist(NetworkIdentifier ni) {
		NodeDescriptor handler;
		try {
			handler = cm.findSuccessor(ni, 0);
			return cm.getRemoteInterface(handler).getIfExist(ni);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("GetIfExists error "+e.getMessage());
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("GetIfExists error "+e.getMessage());
		}
	}

	@Override
	public Serializable take(NetworkIdentifier ni) {
		try {
			NodeDescriptor handler = cm.findSuccessor(ni, 0);
			Serializable ret = cm.getRemoteInterface(handler).take(ni);
			if (ret == null) {
				while (cm.getNotifiedResource() == null) {
					cm.wait();
				}
				ret = cm.getNotifiedResource();
			}
			return ret;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Take error "+e.getMessage());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Take error "+e.getMessage());
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Take error "+e.getMessage());
		}
	}

	@Override
	public Serializable takeIfExist(NetworkIdentifier ni) {
		try {
			NodeDescriptor handler = cm.findSuccessor(ni, 0);
			return cm.getRemoteInterface(handler).takeIfExist(ni);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("TakeIfExists error "+e.getMessage());
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("TakeIfExists error "+e.getMessage());
		}
	}

	@Override
	public void leave() {
		try {
			cm.leave();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Leave error "+e.getMessage());
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Leave error "+e.getMessage());
		}
	}

	public FingerTable getFingerTable() {
		
		return cm.getFingerTable();
	}

	public NodeDescriptor getSelfDescriptor() {
		return cm.getSelfNode();
	}

}