package achord;

import java.io.Serializable;

public class NetworkIdentifier implements Comparable<NetworkIdentifier>,
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3911626039250553368L;
	protected long hash;
	protected String ID;

	public NetworkIdentifier(String ID) {
		this.ID = ID;
		hash = this.calculateHash(ID);
	}

	public String getID() {
		return ID;
	}

	public boolean isInRange(NetworkIdentifier start, NetworkIdentifier end) {
		return Range.isInRange(hash, start.getHash(), end.getHash());

	}

	public long getHash() {
		return this.hash;
	}

	public boolean equals(NetworkIdentifier ni) {
		return this.hash == ni.getHash();
	}

	/*
	 * private long calculateHash(String ID) { double tmpHash = ID.hashCode();
	 * if (tmpHash < 0) { tmpHash += Math.pow(2, 32); } return (long) tmpHash; }
	 */

	// TODO SIMO Calcolo l'hash con la nuova classe
	private long calculateHash(String ID) {
		return (long) Hasher.hashThis(ID, ChordProprerties.getDimension())
				.longValue();
	}

	@Override
	public int compareTo(NetworkIdentifier ni) {
		if (ni.getHash() > this.hash) {
			return 1;
		} else if (ni.getHash() < this.hash) {
			return -1;
		} else {
			return 0;
		}
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "NI "+this.hash;
	}

}
