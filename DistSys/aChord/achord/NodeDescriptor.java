package achord;

import java.io.Serializable;

public class NodeDescriptor extends NetworkIdentifier implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7493679243431998934L;
	private String host;


	public NodeDescriptor(String host,String ID) {
		super("ChordInterface"+ID);
		this.host=host;
	} 
	
	public String getInterfaceName()
	{
		return this.ID;
	}
	
	public String getHost() 
	{
		return host;
	}
	

	
}
