package achord;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

public class FingerTable {

	private int dimension;
	private NodeDescriptor self;
	private ArrayList<Finger> fingers;
	private ChordManager cm;

	public FingerTable(NodeDescriptor self, ChordManager cm) {
		this.self = self;
		this.cm = cm;
		this.dimension = ChordProprerties.getDimension();
		this.initialize();

	}

	private void initialize() {
		fingers = new ArrayList<Finger>(dimension);
		for (int i = 0; i < dimension; i++) {
			fingers.add(new Finger(self, i, self.getHash()));
		}
	}

	public void initialize(NodeDescriptor successor) throws RemoteException,
			NotBoundException {
		fingers = new ArrayList<Finger>(dimension);
		long selfHash = self.getHash();
		Finger toadd = new Finger(null, 0, selfHash);
		toadd.setHandler(successor);
		fingers.add(toadd);
		Finger prec = toadd;
		for (int i = 1; i < dimension; i++) {
			toadd = new Finger(null, i, selfHash);
			if (prec.handler.equals(self)) {
				fingers.add(new Finger(self, i, self.getHash()));
			} else {
				if (Range.isInRange(toadd.start, selfHash,
						prec.handler.getHash())) {
					System.out.println("Adding successor "+selfHash+" "+toadd.start+" "+prec.handler.getHash());
					toadd.setHandler(prec.handler);
				} else {
					NodeDescriptor t=cm.getRemoteInterface(prec.handler).findSuccessor(toadd.start, 0);
					System.out.println(prec.handler+" "+t);
					
					toadd.setHandler(t);
				}
				fingers.add(toadd);
				prec = toadd;
			}

		}
	}

	public NodeDescriptor getClosestPreceding(NetworkIdentifier ni) {
		for (Finger f : fingers) {
			if (f.checkRange(ni)) {
				return f.getHandler();
			}
		}
		throw new RuntimeException("Finger table incompleta");
	}

	public NodeDescriptor getClosestPreceding(long hash) {
		for (Finger f : fingers) {
			if (f.checkRange(hash)) {
				return f.getHandler();
			}
		}
		throw new RuntimeException("Finger table incompleta");
	}

	public boolean updateOnJoin(NodeDescriptor node) {
		boolean result = false;
		for (Finger f : fingers) {
			result = f.updateRange(node);
			System.out.println(result);
			/*
			 * // Esempio per il range 5-13 preferisco l'handler >= 5 tra
			 * l'attuale e il nuovo nodo if (handler.isGreater(node) &&
			 * f.checkRange(node)) { //TODO Fra setHandler f.setHandler(node);
			 * result = true; }
			 */
		}
		return result;
	}

	public boolean updateOnLeave(NodeDescriptor node, NodeDescriptor substitute) {
		boolean ret = false;
		for (Finger f : fingers) {
			if (f.getHandler().equals(node)) {
				ret = true;
				f.setHandler(substitute);
			}
		}
		return ret;
	}

	public ArrayList<NodeDescriptor> getHandlers() {
		ArrayList<NodeDescriptor> ret = new ArrayList<NodeDescriptor>();
		for (Finger f : fingers) {
			ret.add(f.getHandler());
		}
		return ret;
	}

	public ArrayList<Finger> getFingers() {
		return fingers;
	}
}
