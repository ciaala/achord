package achord;

public class Finger {

	public NodeDescriptor handler;
	/**
	 * Incluso
	 */
	public long start;
	/**
	 * Non Incluso
	 */
	public long end;

	public int step;

	public Finger(NodeDescriptor handler, int power,long selfhash) {
		this.handler = handler;
		this.step = power;
		start = Range.start(power, selfhash);
		end = Range.end(power, selfhash);

	}

	 
	
	public boolean checkRange(NetworkIdentifier ni) {
		//TODO Fra rifatto il check
		boolean alpha,beta;
		long hash = ni.getHash();
		alpha = start <= hash;
		beta = end > hash;
		if ( start < end) {
			return alpha && beta;
		} else {
			return alpha || beta;
		}	
	}
	public boolean checkRange(long hash) {
		//TODO Fra rifatto il check
		boolean alpha,beta;
		alpha = start <= hash;
		beta = end > hash;
		if ( start < end) {
			return alpha && beta;
		} else {
			return alpha || beta;
		}	
	}
	public boolean updateRange(NodeDescriptor nd) {
		long actual_diff = Range.diff(start, handler.getHash());
		long new_diff = Range.diff(start, nd.getHash());
		
		if ( actual_diff > new_diff ) {
			handler = nd;
			return true;
		} 
		return false;
	}
	
	public NodeDescriptor getHandler() {
		return handler;
	}

	public void setHandler(NodeDescriptor node) {
		this.handler = node;
	}

	@Override
	public String toString() {
		return "| " + step + " | " + start + " | " + end + " | " + handler
				+ " |";
	}
}


