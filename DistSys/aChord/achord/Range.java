package achord;

public class Range {
	public static final long maxValue = (long) Math.pow(2,
			ChordProprerties.getDimension());

	public static long start(int power, long base) {
		return mod((long) (Math.pow(2, power) + base));
	}

	public static long end(int power, long base) {
		return mod((long) (Math.pow(2, power + 1) + base));
	}

	public static long mod(long v) {
		return v % maxValue;
	}
	
	public static long diff( long first , long second){
		if (second > first ) {
			return second - first;
		} else {
			return maxValue - first + second;
		}
	}
	
	public static boolean isInRange(long value,long start,long end)
	{
		if (start == end) {
			throw new RuntimeException("Wrong range " + start +" "+value+" "+end);
		} else {
			if (start < end) {
				return value >= start && value < end;
			} else {
				return value >= start || value < end;
			}
		}	
	}
	
}