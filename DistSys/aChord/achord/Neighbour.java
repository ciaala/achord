package achord;

import java.io.Serializable;
import java.util.ArrayList;

import achord.gui.Node;

public class Neighbour implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7277352131434647677L;
	private int successorsMaxLength = ChordProprerties.successorsLenght();
	private int predeccessorsMaxLength = ChordProprerties.predecessorsLenght();
	private NodeDescriptor selfNode;
	private ArrayList<NodeDescriptor> successors;
	private ArrayList<NodeDescriptor> predecessors;

	public Neighbour(NodeDescriptor selfNode) {
		this.successors = new ArrayList<NodeDescriptor>();
		this.predecessors = new ArrayList<NodeDescriptor>();
		this.selfNode = selfNode;
	}

	public ArrayList<NodeDescriptor> getPredecessors() {
		return predecessors;
	}

	public ArrayList<NodeDescriptor> getSuccessors() {
		return successors;
	}

	public void setPredecessors(ArrayList<NodeDescriptor> predecessors) {
		this.predecessors = predecessors;
	}

	public void setSuccessors(ArrayList<NodeDescriptor> successors) {
		this.successors = successors;
	}

	public void addSuccessor(NodeDescriptor node) {
		int index = 0;
		NodeDescriptor prec=selfNode;
		for (NodeDescriptor n : successors) {
			if (node.isInRange(prec,n))
				break;
			prec=n;
			index++;
		}
		System.out.println(index + successorsMaxLength);
		if (index <= successorsMaxLength) {
			successors.add(index, node);
			if (successors.size() > successorsMaxLength) {
				successors.remove(successorsMaxLength - 1);
			}
		} else {
			throw new RuntimeException("Adding wrong node");
		}
	}

	public void removeSuccessor(NodeDescriptor node) {
		for (NodeDescriptor n : successors) {
			if (node.equals(n)) {
				successors.remove(n);
			}
		}
	}

	public void addPredecessor(NodeDescriptor node) {
		int index = 0;
		NodeDescriptor succ=selfNode;
		for (NodeDescriptor n : predecessors) {
			if (node.isInRange(n,succ))
				break;
			succ=n;
			index++;
		}
		if (index <= predeccessorsMaxLength) {
			predecessors.add(index, node);
			if (predecessors.size() > predeccessorsMaxLength) {
				predecessors.remove(predeccessorsMaxLength - 1);
			}
		} else {
			throw new RuntimeException("Adding wrong node");
		}
	}

	public void removePredecessor(NodeDescriptor node) {
		for (NodeDescriptor n : predecessors) {
			if (node.equals(n)) {
				predecessors.remove(n);
			}
		}
	}

	/**
	 * 
	 * @param node
	 * @return null quando non ha trovato un nodo p grande altrimenti il suo
	 *         nodo appena pigrande
	 */
	public NodeDescriptor getClosestNode(NetworkIdentifier node) {
		NodeDescriptor prec=selfNode;
		for(NodeDescriptor n:successors)
		{
			if(node.isInRange(prec, n))
				return n;
			prec=n;
		}
		return null;
	}

	public NodeDescriptor getClosestNode(long hash) {
		NodeDescriptor prec=selfNode;
		for(NodeDescriptor n:successors)
		{
			if(Range.isInRange(hash, prec.getHash(), n.getHash()))
				return n;
			prec=n;
		}
		return null;
	}
	
	public NodeDescriptor getLastSuccessor() {
		if (successors.size() > 0) {
			return successors.get(successors.size() - 1);
		} else {
			return selfNode;
		}

	}

	public NodeDescriptor getFirstSuccessor() {
		if (successors.size() > 0) {
			return successors.get(0);
		} else {
			return selfNode;
		}
	}

	public NodeDescriptor getFirstPredecessor() {
		if (successors.size() > 0) {
			//TODO Fra era 1 ho messo 0
			return predecessors.get(0);
		} else {
			return selfNode;
		}

	}

	public void updateSuccessors(NodeDescriptor node) {
		addSuccessor(node);
	}

	public void updatePredecessors(NodeDescriptor node) {
		addPredecessor(node);
	}

	public boolean hasSuccessor(NodeDescriptor node) {
		return successors.contains(node);
	}

	public boolean hasPredecessor(NodeDescriptor node) {
		return predecessors.contains(node);
	}
}
