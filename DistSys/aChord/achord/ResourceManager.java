package achord;

import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.LinkedList;

enum QueueType {
	GET, TAKE;
}

public class ResourceManager {

	private HashMap<NetworkIdentifier, Serializable> resources;
	private ChordManager cm;
	private HashMap<NetworkIdentifier, LinkedList<QueueElement>> queue;
	private NodeDescriptor selfNode;

	public ResourceManager(ChordManager cm) {
		this.cm = cm;
		resources = new HashMap<NetworkIdentifier, Serializable>();
		queue = new HashMap<NetworkIdentifier, LinkedList<QueueElement>>();
		selfNode=cm.getSelfNode();
	}

	public HashMap<NetworkIdentifier, Serializable> getResources()
	{
		return this.resources;
	}
	
	public HashMap<NetworkIdentifier, Serializable> getResources(NetworkIdentifier ni)
	{
		HashMap<NetworkIdentifier, Serializable> ret=new HashMap<NetworkIdentifier, Serializable>();
		
		for(NetworkIdentifier n:resources.keySet())
		{
			if(!n.isInRange(ni,selfNode))
			{
				ret.put(n, resources.get(n));
				resources.remove(n);
			}
		}
		return this.resources;
	}
	
	public synchronized void put(NetworkIdentifier ni, Serializable sr)
			throws RemoteException, NotBoundException {
		if (queue.containsKey(ni)) {
			for (QueueElement q : queue.get(ni)) {
				cm.getRemoteInterface(q.nd).notifyResource(sr);
				if (q.qt == QueueType.TAKE) {
					return;
				}
			}
		} else {
			resources.put(ni, sr);
		}
	}

	public Serializable get(NetworkIdentifier ni) {
		return resources.get(ni);
	}

	public Serializable getIfExist(NetworkIdentifier ni) {
		return resources.get(ni);
	}

	public Serializable take(NetworkIdentifier ni) {
		if (resources.containsKey(ni)) {
			Serializable ret = resources.get(ni);
			resources.remove(ni);
			return ret;
		}
		return null;
	}
}

class QueueElement {
	public QueueType qt;
	public NodeDescriptor nd;

	public QueueElement(QueueType qt, NodeDescriptor nd) {
		this.qt = qt;
		this.nd = nd;
	}
}

