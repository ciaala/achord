/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package achord;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author crypt
 */
public class ChordManager extends UnicastRemoteObject implements ChordStub {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6414286221690701299L;
	private Neighbour nb;
	private FingerTable fingerTable;
	private NodeDescriptor selfNodeDescriptor;
	private String id;
	private ResourceManager rm;
	private HashMap<NodeDescriptor, ChordStub> stubCache = new HashMap<NodeDescriptor, ChordStub>();
	private Serializable notified;

	private static final String policy = ChordProprerties.getPolicy();

	public ChordManager(String selfHost, String id) throws RemoteException {
		super();
		this.rm = new ResourceManager(this);
		this.notified = null;
		selfNodeDescriptor = new NodeDescriptor(selfHost, id);
		nb = new Neighbour(this.selfNodeDescriptor);
		this.fingerTable = new FingerTable(this.selfNodeDescriptor, this);
		// this.routingManager = new RoutingManager(selfNodeDescriptor,this);
		this.id = id;
	}

	public void start() throws RemoteException, MalformedURLException {
		System.out.println("Starting Connection");
		SecurityManager sm = System.getSecurityManager();
		if (sm == null) {
			Logger.getLogger(ChordManager.class.getName()).log(Level.INFO,
					"SecurityManager Null", sm);

			System.setProperty("java.security.policy", policy);

			System.setSecurityManager(new SecurityManager());
		}
		Registry registry = LocateRegistry.getRegistry();
		registry.rebind("ChordInterface" + id, this);
		System.out.println("Bounded ! " + "ChordInterface" + id);
	}

	/*
	 * public void joinNetwork(String remoteHost, String remoteID) throws
	 * RemoteException, NotBoundException { Registry registry =
	 * LocateRegistry.getRegistry(remoteHost); ChordInterface stub =
	 * getRemoteInterface(new NodeDescriptor(remoteHost, remoteID));
	 * stub.join(selfNodeDescriptor); }
	 */

	public synchronized NodeDescriptor getSelfNode() {
		return selfNodeDescriptor;
	}

	@Override
	public synchronized Serializable get(NetworkIdentifier ni)
			throws RemoteException {
		this.notified = null;
		return rm.get(ni);
	}

	@Override
	public synchronized void put(NetworkIdentifier ni, Serializable sr)
			throws RemoteException, NotBoundException {
		rm.put(ni, sr);
	}

	@Override
	public synchronized Serializable getIfExist(NetworkIdentifier ni)
			throws RemoteException {
		return rm.get(ni);
	}

	@Override
	public synchronized Serializable take(NetworkIdentifier ni)
			throws RemoteException {
		return rm.take(ni);
	}

	@Override
	public synchronized Serializable takeIfExist(NetworkIdentifier ni)
			throws RemoteException {
		return rm.take(ni);
	}

	public synchronized void join(String remoteHost, String remoteID)
			throws RemoteException, NotBoundException {
		NodeDescriptor handler = getRemoteInterface(
				new NodeDescriptor(remoteHost, remoteID)).findSuccessor(
				selfNodeDescriptor, 0);
		ChordStub stub = getRemoteInterface(handler);
		nb = stub.join(selfNodeDescriptor);
		fingerTable.initialize(nb.getFirstSuccessor());
		updateStubCache();
	}

	private synchronized void updateStubCache() throws RemoteException,
			NotBoundException {
		for (NodeDescriptor n : nb.getPredecessors()) {
			addRemoteInterface(n);
		}
		for (NodeDescriptor n : nb.getSuccessors()) {
			addRemoteInterface(n);
		}
		for (NodeDescriptor n : fingerTable.getHandlers()) {
			addRemoteInterface(n);
		}
	}

	@Override
	public synchronized Neighbour join(NodeDescriptor node)
			throws RemoteException, NotBoundException {
		Neighbour ret = new Neighbour(node);
		ret.addSuccessor(selfNodeDescriptor);
		ret.addPredecessor(nb.getFirstPredecessor());
		HashMap<NetworkIdentifier, Serializable> splitResource = rm
				.getResources(node);
		// TODO Ridistribuzione risorse
		if (splitResource.size() > 0)
			getRemoteInterface(node).put(splitResource);
		this.updateOnJoin(node);
		return ret;
		/*
		 * NodeDescriptor closest = nb.getClosestNode(node); if (closest ==
		 * null) { closest = fingerTable.getClosestPreceding(node); if
		 * (!closest.equals(selfNodeDescriptor)) { this.updateOnJoin(node);
		 * return getRemoteInterface(closest).join(node); } else { Neighbour ret
		 * = new Neighbour(node); ret.addSuccessor(nb.getFirstSuccessor());
		 * ret.addPredecessor(nb.getFirstPredecessor()); // TODO Ridistribuzione
		 * risorse this.updateOnJoin(node); return ret; } } else { Neighbour ret
		 * = new Neighbour(node); ret.addSuccessor(nb.getFirstSuccessor());
		 * ret.addPredecessor(nb.getFirstPredecessor()); // TODO Ridistribuzione
		 * risorse this.updateOnJoin(node); return ret; }
		 */
	}

	public synchronized void addRemoteInterface(NodeDescriptor node)
			throws RemoteException, NotBoundException {
		if (!stubCache.containsKey(node)) {
			Registry registry = LocateRegistry.getRegistry(node.getHost());
			ChordStub stub = (ChordStub) registry.lookup(node
					.getInterfaceName());
			stubCache.put(node, stub);
		}

	}

	public synchronized ChordStub getRemoteInterface(NodeDescriptor node)
			throws RemoteException, NotBoundException {
		if (stubCache.containsKey(node)) {
			return stubCache.get(node);
		}
		Registry registry = LocateRegistry.getRegistry(node.getHost());
		System.out.println("Getting interface " + node.getInterfaceName());
		ChordStub stub = (ChordStub) registry.lookup(node.getInterfaceName());
		stubCache.put(node, stub);
		return stub;
	}

	@Override
	public synchronized NodeDescriptor getSuccessor() {
		return nb.getFirstSuccessor();
	}

	@Override
	public synchronized NodeDescriptor getPredecessor() {
		return nb.getFirstPredecessor();
	}

	@Override
	public synchronized NodeDescriptor findSuccessor(NetworkIdentifier ni,
			int hopCounter) throws RemoteException, NotBoundException {
		NodeDescriptor closest = nb.getClosestNode(ni);
		if (closest == null) {
			closest = fingerTable.getClosestPreceding(ni);
			if (!closest.equals(selfNodeDescriptor)) {
				closest = this.getRemoteInterface(closest).findSuccessor(ni,
						hopCounter + 1);
			}
		}
		return closest;
	}

	@Override
	public synchronized void updateOnJoin(NodeDescriptor node)
			throws RemoteException, NotBoundException {
		nb.updateSuccessors(node);
		nb.updatePredecessors(node);
		if (fingerTable.updateOnJoin(node)) {

			for (NodeDescriptor n : fingerTable.getHandlers()) {
				if (!n.equals(selfNodeDescriptor) && !node.equals(n))
					getRemoteInterface(n).updateOnJoin(node);
			}
		}
		updateStubCache();
		System.out.println("Update on Join completed");
	}

	@Override
	public synchronized void updateOnLeave(NodeDescriptor node,
			NodeDescriptor substitute) throws RemoteException,
			NotBoundException {
		// TODO Auto-generated method stub
		nb.updateSuccessors(node);
		nb.updatePredecessors(node);
		if (fingerTable.updateOnLeave(node, substitute)) {
			for (NodeDescriptor n : fingerTable.getHandlers()) {
				getRemoteInterface(n).updateOnLeave(node, substitute);
			}
		}
	}

	@Override
	public synchronized void notifyResource(Serializable sr)
			throws RemoteException {
		this.notified = sr;
		this.notifyAll();
	}

	public synchronized Serializable getNotifiedResource() {
		Serializable result = this.notified;
		return result;
	}

	@Override
	public synchronized void put(HashMap<NetworkIdentifier, Serializable> res)
			throws RemoteException, NotBoundException {
		for (NetworkIdentifier ni : res.keySet()) {
			rm.put(ni, res.get(ni));
		}
	}

	public void leave() throws RemoteException, NotBoundException {
		getRemoteInterface(nb.getFirstSuccessor()).leaveOnSuccessor(
				selfNodeDescriptor, nb, rm.getResources());
		getRemoteInterface(nb.getFirstPredecessor()).leaveOnPredecessor(
				selfNodeDescriptor, nb);
	}

	@Override
	public synchronized boolean leaveOnSuccessor(NodeDescriptor node,
			Neighbour remotenb,
			HashMap<NetworkIdentifier, Serializable> resources)
			throws RemoteException, NotBoundException {
		if (nb.getFirstPredecessor().equals(node)) {
			nb.setPredecessors(remotenb.getPredecessors());
			put(resources);
			if (fingerTable.updateOnLeave(node, remotenb.getFirstSuccessor())) {
				for (NodeDescriptor n : fingerTable.getHandlers()) {
					getRemoteInterface(n).updateOnLeave(node,
							remotenb.getFirstSuccessor());
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public synchronized boolean leaveOnPredecessor(NodeDescriptor node,
			Neighbour remotenb) throws RemoteException, NotBoundException {
		if (nb.getFirstSuccessor().equals(node)) {
			nb.setSuccessors(remotenb.getSuccessors());
			if (fingerTable.updateOnLeave(node, remotenb.getFirstSuccessor())) {
				for (NodeDescriptor n : fingerTable.getHandlers()) {
					getRemoteInterface(n).updateOnLeave(node,
							remotenb.getFirstSuccessor());
				}
			}
			return true;
		}
		return false;
	}

	public FingerTable getFingerTable() {

		return fingerTable;
	}

	@Override
	public NodeDescriptor findSuccessor(long hash, int hop)
			throws RemoteException, NotBoundException {
		NodeDescriptor closest = nb.getClosestNode(hash);
		System.out.println(closest);
		if (closest == null) {
			closest = fingerTable.getClosestPreceding(hash);
			System.out.println(closest);
			if (!closest.equals(selfNodeDescriptor)) {
				closest = this.getRemoteInterface(closest).findSuccessor(hash,
						hop + 1);
			}
		}
		return closest;
	}

}
