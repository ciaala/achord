/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package achord;

import java.io.Serializable;
import java.rmi.Remote;

/**
 * 
 * @author crypt
 */
public interface ChordInterface extends Remote {
	public void put(NetworkIdentifier ni, Serializable sr);

	public Serializable get(NetworkIdentifier ni);

	public Serializable getIfExist(NetworkIdentifier ni);

	public Serializable take(NetworkIdentifier ni);

	public Serializable takeIfExist(NetworkIdentifier ni);

	public void join(String remoteHost, String remoteID);

	public void leave();

}
