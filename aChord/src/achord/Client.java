/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package achord;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author crypt
 */
public class Client {

    public Client() {
    }

    public ChordInterface getRemoteInterface(String host) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry(host);
        ChordInterface stub = (ChordInterface) registry.lookup("ChordInterface");
        return stub;
    }

    public static void main(String[] args) {
        System.out.println("Starting Client");
        String host = args.length < 1 ? null : args[0];
        long time = -1;
        if (host != null) {
            try {
                Client c = new Client();
                ChordInterface cif = (c.getRemoteInterface(host));
                time = cif.get();
                System.out.println("Remote time in millis " + time);
            } catch (RemoteException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotBoundException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
