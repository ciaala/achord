/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package achord;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author crypt
 */
public class Receiver extends UnicastRemoteObject implements ChordInterface{

    public Receiver() throws RemoteException{
        super();
    }
    public void start() throws RemoteException, MalformedURLException{
        System.out.println("Starting Connection");

        SecurityManager sm = System.getSecurityManager();
        if ( sm == null ){
            Logger.getLogger(Receiver.class.getName()).log(Level.INFO, "SecurityManager Null", sm);
            
            System.setProperty("java.security.policy", "policy.txt");
            System.setSecurityManager(new SecurityManager());
        }
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind("ChordInterface", this);
        System.out.println("Bounded !");
    }
    
    @Override
    public long get() throws RemoteException {
        Calendar c = Calendar.getInstance();
        return System.currentTimeMillis();
    }
    
}
