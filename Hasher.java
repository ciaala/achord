import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;


public class Hasher {
   
    private MessageDigest aes;
    private BigInteger modulus;
    
    public Hasher(int exponent) {
	
	modulus = new BigInteger("2");
	modulus = modulus.pow(exponent);
	try {
	    aes = MessageDigest.getInstance("MD5");
	} catch (NoSuchAlgorithmException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }
    public BigInteger hashThis(String thiz) {
	BigInteger result;
	aes.update(thiz.getBytes());
	result = new BigInteger(1, aes.digest());
	result = result.mod(modulus);
	return result;
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
	HashSet<BigInteger> set = new HashSet<BigInteger>();
	int duplicate = 0;
	Hasher hashIt = new Hasher(11);
	for (int i =0; i <2048; i++ ) {
	    BigInteger value = hashIt.hashThis("ChordInterface"+i);
	    if ( set.contains(value) ){
		duplicate ++;
		System.out.println(i +": "+value);
	    } else {
		set.add(value);
	    }
	}
	System.out.println(duplicate);

	return;
    }

}
